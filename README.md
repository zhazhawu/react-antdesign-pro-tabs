
<h1 align="center">Ant Design Pro React</h1>
<div align="center">
自学的前端，渣渣小趴菜一个，别喷，自用的
</div>



Overview
----

![dashboard + multi-tabs](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images1.png)
![dashboard + setting](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images2.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images3.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images4.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images5.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images6.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images7.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images8.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images9.png)
![user profile](https://gitee.com/zhazhawu/preview/raw/master/src/assets/images10.png)




### Env and dependencies

- node
- mock
- webpack
- eslint
- @vue/cli ~3
- [ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 


### Project setup

- Clone repo
```bash
git clone https://gitee.com/zhazhawu/ant-design-pro-vue.git
cd ant-design-pro-vue
```

- Install dependencies
```
npm install
```

- Compiles and hot-reloads for development
```
npm start
```

- Compiles and minifies for production
```
npm run build
```


### Other



## Browsers support

Modern browsers and IE10.

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE / Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Opera" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Opera |
| --- | --- | --- | --- | --- |
| IE10, Edge | last 2 versions | last 2 versions | last 2 versions | last 2 versions |

