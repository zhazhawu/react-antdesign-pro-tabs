import Mock from 'mockjs2'
import {builder} from '../util'

import { Outlet } from "react-router-dom";
import {CheckCircleOutlined, FormOutlined, HomeOutlined, TableOutlined, WarningOutlined} from "@ant-design/icons"
import asyncComponent from "@/utils/asyncComponent";

const info = (options) => {
    const userInfo = {
        'id': '4291d7da9005377ec9aec4a71ea837f',
        'name': '张三',
        'username': 'admin',
        'password': '',
        'avatar': 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png',
        'status': 1,
        'telephone': '',
        'lastLoginIp': '27.154.74.117',
        'lastLoginTime': 1534837621348,
        'creatorId': 'admin',
        'createTime': 1497160610259,
        'merchantCode': 'TLif2btpzg079h15bk',
        'deleted': 0,
        'roleId': 'admin',
        'role': {}
    }
    // role
    const roleObj = {
        'id': 'admin',
        'name': '管理员',
        'describe': '拥有所有权限',
        'status': 1,
        'creatorId': 'system',
        'createTime': 1497160610259,
        'deleted': 0,
		'route': [
			{
				'key':'Home',
				'index': true,
				'rule': '/',
				'jump': '/',
				'element': <Outlet/>,
				'icon':<HomeOutlined/>,
				'title': '主页'
			},
			{
				'key':'Form',
				'rule': 'form',
				'element': <Outlet/>,
				'icon':<FormOutlined/>,
				'title': '表单',
				'jump': '/Form/base-form',
				'children':[
					{
						'key':'base-form',
						'rule':'base-form',
						'jump':'/Form/base-form',
						'element': asyncComponent(()=>import('@/views/form/BaseForm')),
						'title': '基础表单'
					},
					{
						'key':'step-form',
						'rule': 'step-form',
						'jump':'/Form/step-form',
						'element': asyncComponent(()=>import('@/views/form/StepsForm')),
						'title': '分步表单'
					},
				]
			},
			{
				'key':'List',
				'rule': 'list',
				'element': <Outlet/>,
				'icon':<TableOutlined/>,
				'title': '列表',
				'jump': '/List/table-list',
				'children':[
					{
						'key':'table-list',
						'rule':'table-list',
						'jump':'/List/table-list',
						'element': asyncComponent(()=>import('@/views/list/TableList')),
						'title': '查询表格'
					},
					{
						'key':'three-table',
						'rule': 'three-table',
						'jump':'/List/three-table',
						'element': asyncComponent(()=>import('@/views/list/ThreeTable')),
						'title': '树形表格'
					},
					{
						'key':'three-table2',
						'rule': 'three-table2',
						'jump':'/List/three-table2',
						'element': asyncComponent(()=>import('@/views/list/ThreeTable2')),
						'title': '横向树形表格'
					},
					{
						'key':'card',
						'rule': 'card',
						'jump':'/List/card',
						'element': asyncComponent(()=>import('@/views/list/CardList')),
						'title': '卡片列表'
					},
				]
			},
			{
				'key':'Result',
				'rule': 'result',
				'element': <Outlet/>,
				'icon':<CheckCircleOutlined/>,
				'title': '列表',
				'jump':'/Result/success',
				'children':[
					{
						'key':'success',
						'rule':'success',
						'jump':'/Result/success',
						'element': asyncComponent(()=>import('@/views/resut/Success')),
						'title': '成功页'
					},
					{
						'key':'fail',
						'rule':'fail',
						'jump':'/Result/fail',
						'element': asyncComponent(()=>import('@/views/resut/Error')),
						'title': '失败页'
					},
				]
			},
			{
				'key':'Exception',
				'rule': 'exception',
				'element': <Outlet/>,
				'icon':<WarningOutlined/>,
				'title': '异常页',
				'jump':'/Exception/403',
				'children':[
					{
						'key':'403',
						'rule':'403',
						'jump':'/Exception/403',
						'element': asyncComponent(()=>import('@/views/exception/403')),
						'title': '403'
					},
					{
						'key':'404',
						'rule':'404',
						'jump':'/Exception/404',
						'element': asyncComponent(()=>import('@/views/exception/404')),
						'title': '404'
					},
					{
						'key':'500',
						'rule':'500',
						'jump':'/Exception/500',
						'element': asyncComponent(()=>import('@/views/exception/500')),
						'title': '500'
					},

				]
			},
		],
        'permissions': [{
            'roleId': 'admin',
            'permissionId': 'dashboard',
            'permissionName': '仪表盘',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'exception',
            'permissionName': '异常页面权限',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'result',
            'permissionName': '结果权限',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'profile',
            'permissionName': '详细页权限',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'table',
            'permissionName': '表格权限',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'import',
                'describe': '导入',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'form',
            'permissionName': '表单权限',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'order',
            'permissionName': '订单管理',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'permission',
            'permissionName': '权限管理',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'role',
            'permissionName': '角色管理',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'table',
            'permissionName': '桌子管理',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'query',
                'describe': '查询',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }, {
            'roleId': 'admin',
            'permissionId': 'user',
            'permissionName': '用户管理',
            'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
            'actionEntitySet': [{
                'action': 'add',
                'describe': '新增',
                'defaultCheck': false
            }, {
                'action': 'import',
                'describe': '导入',
                'defaultCheck': false
            }, {
                'action': 'get',
                'describe': '详情',
                'defaultCheck': false
            }, {
                'action': 'update',
                'describe': '修改',
                'defaultCheck': false
            }, {
                'action': 'delete',
                'describe': '删除',
                'defaultCheck': false
            }, {
                'action': 'export',
                'describe': '导出',
                'defaultCheck': false
            }],
            'actionList': null,
            'dataAccess': null
        }]
    }

    roleObj.permissions.push({
        'roleId': 'admin',
        'permissionId': 'support',
        'permissionName': '超级模块',
        'actions': '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
        'actionEntitySet': [{
            'action': 'add',
            'describe': '新增',
            'defaultCheck': false
        }, {
            'action': 'import',
            'describe': '导入',
            'defaultCheck': false
        }, {
            'action': 'get',
            'describe': '详情',
            'defaultCheck': false
        }, {
            'action': 'update',
            'describe': '修改',
            'defaultCheck': false
        }, {
            'action': 'delete',
            'describe': '删除',
            'defaultCheck': false
        }, {
            'action': 'export',
            'describe': '导出',
            'defaultCheck': false
        }],
        'actionList': null,
        'dataAccess': null
    })

    userInfo.role = roleObj
    return builder(userInfo)
}

const userNav = () => {
    const nav = [
  //   {
  //       key:'Home',
  //       index: true,
  //       path: '/',
		// icon: 'HomeOutlined',
		// title: '主页', 
		// meta: {
		// 	title: '主页', 
		// },
  //   },
	{
		key:'Home',
		icon: 'HomeOutlined',
		path: '/Home/dashboard',
		title: '平台', 
		meta: {
			title: '平台', 
			icon: '<FormOutlined/>',
		},
		children:[
			{
				key:'dashboard',
				path:'/Home/dashboard',
				title: '首页',
				meta: {
					title: '首页', 
				},
			},
			{
				key:'analysis',
				path:'/Home/analysis',
				title: '分析',
				meta: {
					title: '分析', 
				},
			},
		]
	},
    {
        key:'Form',
		icon: 'FormOutlined',
		path: '/Form/base-form',
		title: '表单', 
		meta: {
			title: '表单', 
			icon: '<FormOutlined/>',
		},
        children:[
            {
                key:'base-form',
                path:'/Form/base-form',
                title: '基础表单',
				meta: {
					title: '基础表单', 
				},
            },
            {
                key:'step-form',
                path:'/Form/step-form',
                title: '分步表单',
				meta: {
					title: '分步表单', 
				},
            },
        ]
    },
    {
        key:'List',
		icon: 'TableOutlined',
		path: '/List/table-list',
		title: '列表', 
		meta: {
			title: '列表', 
		},
        children:[
            {
                key:'table-list',
                path:'/List/table-list',
                title: '查询表格',
				meta: {
					title: '查询表格', 
				},
            },
            {
                key:'three-table',
                path:'/List/three-table',
                title: '树形表格',
				meta: {
					title: '树形表格', 
				},
            },
            {
                key:'three-table2',
                path:'/List/three-table2',
                title: '横向树形表格',
				meta: {
					title: '横向树形表格', 
				},
            },
            {
                key:'card',
                path:'/List/card',
                title: '卡片列表',
				meta: {
					title: '卡片列表', 
				},
            },
        ]
    },
    {
        key:'Result',
		icon: 'CheckCircleOutlined',
		path: '/Result/success',
		title: '详情页', 
		meta: {
			title: '详情页', 
		},
        children:[
            {
                key:'success',
                path:'/Result/success',
                title: '成功页',
				meta: {
					title: '成功页', 
				},
            },
            {
                key:'fail',
                path:'/Result/fail',
                title: '失败页',
				meta: {
					title: '失败页', 
				},
            },
        ]
    },
    {
        key:'Exception',
		icon: 'WarningOutlined',
		path: '/Exception/403',
		title: '异常页',
		meta: {
			title: '异常页', 
		},
        children:[
            {
                key:'403',
                path:'/Exception/403',
                title: '403',
				meta: {
					title: '403', 
				},
            },
            {
                key:'404',
                path:'/Exception/404',
                title: '404',
				meta: {
					title: '404', 
				},
            },
            {
                key:'500',
                path:'/Exception/500',
                title: '500',
				meta: {
					title: '500', 
				},
            },

        ]
    },
]
    const json = builder(nav)
    console.log('json', json)
    return json
}

Mock.mock(/\/user\/info/, 'get', info)
Mock.mock(/\/user\/nav/, 'get', userNav)
