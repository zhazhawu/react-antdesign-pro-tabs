import { useEffect, useCallback, useState, Suspense } from 'react';
import { ConfigProvider, Layout, PageHeader } from 'antd';
import {Link, Outlet, useLocation} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import locale from 'antd/lib/locale/zh_CN';
import { connect } from "react-redux"
//import SiderMenu from "../components/GlobalHeader/SiderMenu";
import LeftMenu from '../components/GlobalHeader/LeftMenu';
import 'moment/locale/zh-cn';
import "./BasicLayout.less"
import Menu from '../components/GlobalHeader/Menu';
import GlobalHeader from "../components/GlobalHeader/GlobalHeader";
import TagsView from '../components/tagView';
import { RouterMap } from "@/config/route.config";
const { Sider, Content } = Layout;
	
const BasicLayout = () => {
	const { theme, layoutMode, collapsed } = useSelector(state => state.app);
	const { route, memuData, firstMenu, subMenu } = useSelector(state => state.user);
	
	const SiderMenu = () => {
			return (
				<>
					<Sider
						id='basic-layout-side'
						width={80}
						trigger={null}
						collapsible
						collapsed={collapsed}
						collapsedWidth={60}
						theme="dark"
						className="leftmenu"
					>
						<div className="logo" style={{padding: `16px ${!collapsed ? 16 : 8}px`}}>
							<Link to='/'>
								<img src="https://iczer.gitee.io/vue-antd-admin/static/img/logo.9652507e.png" alt="es-lint want to get"/>
							</Link>
						</div>
						<LeftMenu route={route} memuData={memuData} theme="dark" />
					</Sider>
					<Sider
						className="layout-side"
						width={256}
						trigger={null}
						collapsible
						collapsed={collapsed}
						collapsedWidth={60}
						theme="light"
					>
						<div className="logo" style={{padding: `16px ${!collapsed ? 16 : 8}px`}}>
							<Link to='/'>
								{!collapsed ? <h1 className={ theme !== 'light' ? 'theme-light-title' : 'theme-dark-title'}>Ant Design Pro</h1> : null}
							</Link>
						</div>
						<Menu route={route} memuData={subMenu} theme="light"/>
					</Sider>
				</>
				
			);
		};
	
	return (
		<ConfigProvider locale={locale} >
			<Layout className="flex" id="Basic-Layout" style={{transition: '0.2s all'}}>
				{layoutMode !== 'topmenu' && layoutMode !== 'mixside' && layoutMode !== 'mix' ? 
				<Sider
					id='basic-layout-side'
					width={256}
					trigger={null}
					collapsible
					collapsed={collapsed}
					collapsedWidth={60}
					theme={theme}
				>
					<div className="logo" style={{padding: `16px ${!collapsed ? 16 : 8}px`}}>
						<Link to='/'>
							<img src="https://iczer.gitee.io/vue-antd-admin/static/img/logo.9652507e.png" alt="es-lint want to get"/>
							{!collapsed ? <h1 className={ theme === 'light' ? 'theme-light-title' : ''}>Ant Design Pro</h1> : null}
						</Link>
					</div>
					<Menu route={route} memuData={layoutMode === 'mixmenu' ? subMenu : memuData} theme={theme} />
				</Sider>
				: null}
				
				{layoutMode === 'mixside' || layoutMode === 'mix'? <SiderMenu/> : null}
				
				<Layout className="flex-1 flex flex-column">
					<GlobalHeader route={route} memuData={layoutMode === 'mixmenu' ? firstMenu : memuData}/>
					<Content className="layout-content flex-1 flex flex-column">
						<TagsView />
						<Suspense className="flex-1" fallback={null}>
							<Outlet />
						</Suspense>
					</Content>
				</Layout>
			</Layout>
		</ConfigProvider>
	);
}


export default BasicLayout
