import { Outlet } from "react-router-dom";
import {CheckCircleOutlined, FormOutlined, HomeOutlined, TableOutlined, WarningOutlined} from "@ant-design/icons"
import asyncComponent from "../utils/asyncComponent";

export const RouterMap =  [
  //   {
  //       key:'Home',
  //       index: true,
  //       rule: '/',
  //       jump: '/',
  //       element: asyncComponent(()=>import('@/views/dashboard')),
  //       icon:<HomeOutlined/>,
  //       title: '主页'
		
  //   },
	{
		key:'Home',
		index: true,
		rule: 'Home',
		element: <Outlet/>,
		icon:<HomeOutlined/>,
		title: '平台',
		jump: '/Home/dashboard',
		children:[
			{
				key:'dashboard',
				rule:'dashboard',
				jump:'/Home/dashboard',
				element: asyncComponent(()=>import('@/views/form/BaseForm')),
				title: '首页'
			},
			{
				key:'analysis',
				rule: 'analysis',
				jump:'/Home/analysis',
				element: asyncComponent(()=>import('@/views/form/StepsForm')),
				title: '分析'
			},
		]
	},
    {
        key:'Form',
        rule: 'form',
        element: <Outlet/>,
        icon:<FormOutlined/>,
        title: '表单',
		jump: '/Form/base-form',
        children:[
            {
                key:'base-form',
                rule:'base-form',
                jump:'/Form/base-form',
                element: asyncComponent(()=>import('@/views/form/BaseForm')),
                title: '基础表单'
            },
            {
                key:'step-form',
                rule: 'step-form',
                jump:'/Form/step-form',
                element: asyncComponent(()=>import('@/views/form/StepsForm')),
                title: '分步表单'
            },
        ]
    },
    {
        key:'List',
        rule: 'list',
        element: <Outlet/>,
        icon:<TableOutlined/>,
        title: '列表',
		jump: '/List/table-list',
        children:[
            {
                key:'table-list',
                rule:'table-list',
                jump:'/List/table-list',
                element: asyncComponent(()=>import('@/views/list/TableList')),
                title: '查询表格'
            },
            {
                key:'three-table',
                rule: 'three-table',
                jump:'/List/three-table',
                element: asyncComponent(()=>import('@/views/list/ThreeTable')),
                title: '树形表格'
            },
            {
                key:'three-table2',
                rule: 'three-table2',
                jump:'/List/three-table2',
                element: asyncComponent(()=>import('@/views/list/ThreeTable2')),
                title: '横向树形表格'
            },
            {
                key:'card',
                rule: 'card',
                jump:'/List/card',
                element: asyncComponent(()=>import('@/views/list/CardList')),
                title: '卡片列表'
            },
        ]
    },
    {
        key:'Result',
        rule: 'result',
        element: <Outlet/>,
        icon:<CheckCircleOutlined/>,
        title: '列表',
		jump:'/Result/success',
        children:[
            {
                key:'success',
                rule:'success',
                jump:'/Result/success',
                element: asyncComponent(()=>import('@/views/resut/Success')),
                title: '成功页'
            },
            {
                key:'fail',
                rule:'fail',
                jump:'/Result/fail',
                element: asyncComponent(()=>import('@/views/resut/Error')),
                title: '失败页'
            },
        ]
    },
    {
        key:'Exception',
        rule: 'exception',
        element: <Outlet/>,
        icon:<WarningOutlined/>,
        title: '异常页',
		jump:'/Exception/403',
        children:[
            {
                key:'403',
                rule:'403',
                jump:'/Exception/403',
                element: asyncComponent(()=>import('@/views/exception/403')),
                title: '403'
            },
            {
                key:'404',
                rule:'404',
                jump:'/Exception/404',
                element: asyncComponent(()=>import('@/views/exception/404')),
                title: '404'
            },
            {
                key:'500',
                rule:'500',
                jump:'/Exception/500',
                element: asyncComponent(()=>import('@/views/exception/500')),
                title: '500'
            },

        ]
    },
]
