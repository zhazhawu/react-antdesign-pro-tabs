import { createSlice } from "@reduxjs/toolkit"
import storage from "store";
import {DEFAULT_COLOR, DEFAULT_LAYOUT_MODE, DEFAULT_THEME} from "../mutation-types";

export const app = createSlice({
    name:'app',
    initialState:{
        theme: 'dark',
        layoutMode: 'sidemenu',
        color: '#1890FF',
        collapsed: false,
		setting: false,
    },
    reducers: {
		toggleSetting: (state,{ payload })=>{
		    state.setting = payload
		},
        toggleCollapsed: (state,{ payload })=>{
            state.collapsed = payload
        },
        toggleTheme: (state,{ payload })=>{
            if (!payload){
                payload = 'dark'
            }
            storage.set(DEFAULT_THEME, payload)
            state.theme = payload
        },
        toggleColor: (state, { payload }) => {
            if (!payload){
                payload = '#1890FF'
            }
            storage.set(DEFAULT_COLOR, payload)
            state.color = payload
        },
        toggleLayoutMode: (state, { payload }) => {
            if (!payload){
                payload = 'sidemenu'
            }
            storage.set(DEFAULT_LAYOUT_MODE, payload)
            state.layoutMode = payload
        },
    }
})

export const { toggleSetting, toggleCollapsed, toggleTheme, toggleColor, toggleLayoutMode } = app.actions

export default app.reducer
