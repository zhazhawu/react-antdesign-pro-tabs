import {createSlice, createAsyncThunk} from "@reduxjs/toolkit"
import {ACCESS_TOKEN,USERINFOR} from "../mutation-types";
import {login, logout, getInfo, getMenuList} from "@/api/user"
import storage from 'store'
import {treeToArray} from "@/utils/utils";

export const Login = createAsyncThunk('user/Login', async (param) => {
    const result = login(param)
    return result
})

export const Logout = createAsyncThunk('user/Logout',async (token) => {
	//console.log("======Logout====",param)
    const result = logout(token)
    return result
})

export const GetInfo = createAsyncThunk('user/GetInfo', async (param) => {
    const result = await getInfo(param)
    return result
})

export const GetMenu = createAsyncThunk('user/getMenuList', async () => {
    const result = await getMenuList()
    return result
})

export const user = createSlice({
    name: 'user',
    initialState: {
        token: storage.get(ACCESS_TOKEN) || '',
        roles: [],
        info: storage.get(USERINFOR) || {},
		route: storage.get("route") || [],
		memuData: storage.get("memuData") || [],
		firstMenu: storage.get("firstMenu") || [],
		subMenu: storage.get("subMenu") || [],
		menuList: storage.get('menuList'),
    },
    reducers: {
        setToken: (state, {payload}) => {
            if (!payload) {
                payload = ''
            } else {
                storage.set(ACCESS_TOKEN, payload)
            }
            state.token = payload
        },
		setRole: (state, {payload}) => {
		    state.roles = payload.result.role
		},
		resetUser: (state, {payload}) => {
		    state.info = ''
		},
		setMenu: (state, {payload}) => {
			console.log("=======state.payloadpayloadpayload=========",payload)
			state.subMenu = payload
			storage.set("subMenu",payload)
		},
		setUserItem(state, action) {
			Object.assign(state, action.payload);
		},
    },
    extraReducers: {
        [Login.fulfilled]: (state, {payload}) => {
			console.log("======payload====",payload)
			const token = payload.result.token
			state.token = token
			storage.set(ACCESS_TOKEN, token, 7 * 24 * 60 * 60 * 1000)
        },
        [Logout.fulfilled]: (state, {payload}) => {
			console.log("======Logout====",payload)
            state.token = ''
			storage.remove(ACCESS_TOKEN)
			storage.remove(USERINFOR)
			storage.remove("route")
			storage.remove("memuData")
			storage.remove("firstMenu")
			storage.remove("subMenu")
			storage.remove('menuList')
			//window.location.href = '/user/login'
			window.location.reload()
        },
        [GetInfo.fulfilled]: (state, { payload }) => {
			console.log("======GetInfo====",payload)
            state.roles = payload.result.role
            state.info = payload.result
			storage.set(USERINFOR,payload.result)
			
        },
        [GetInfo.rejected]: (state, data) => {
            console.log(data)
        },
		
		[GetMenu.fulfilled]: (state, { payload }) => {
			console.log("======GetMenu====",payload)
			const firstMenu = payload.result.map(item => {
				const menuItem = {...item}
				delete menuItem.children
				return menuItem
			})
			state.route = treeToArray(payload.result)
			state.memuData = payload.result
			state.firstMenu = firstMenu
			storage.set('route', treeToArray(payload.result))
			storage.set('firstMenu', firstMenu)
			storage.set('memuData', payload.result)
			storage.set('menuList', payload.result)
		},
    }
})

export const {setToken,resetUser,setRole,setMenu, setUserItem} = user.actions

export default user.reducer
