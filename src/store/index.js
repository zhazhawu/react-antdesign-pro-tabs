import { configureStore } from "@reduxjs/toolkit"
import app from './modular/app'
import user from './modular/user'
import tagsView from './modular/tagsView'
export default configureStore({
    reducer:{
        app,
        user,
        tagsView
    }
})
