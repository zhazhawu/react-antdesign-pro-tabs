import React from 'react';

export default function asyncComponent(importComponent) {
    class AsyncComponent extends React.Component {
        constructor() {
            super();

            this.state = {
                component: null
            }
        }

        async componentDidMount() {
            const { default: component } = await importComponent();

            this.setState({component: component})
        }

        render() {
            const Element = this.state.component;

            return Element ? <Element {...this.props} /> : null;
        }
    }

    return <AsyncComponent/>;
}
