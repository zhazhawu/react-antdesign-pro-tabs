export const treeToArray = tree => {
    let arr= [];
    const expanded = datas => {
        if (datas&& datas.length > 0){
            datas.forEach(e => {
                arr.push(e);
                expanded(e.children);
            })
        }
    };
    expanded(tree);
    return arr;
}

