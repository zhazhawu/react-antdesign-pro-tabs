import {ACCESS_TOKEN} from "@/store/mutation-types";
import storage from 'store'

export function getToken() {
  return storage.get(ACCESS_TOKEN)
}

export function setToken(token) {
  return storage.set(ACCESS_TOKEN, token)
}

export function removeToken() {
  return storage.remove(ACCESS_TOKEN)
}
