import React,{Component} from "react";
import {Button, Form, Input, message} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import { connect } from "react-redux"
import {Navigate} from "react-router-dom"
import { Login as doLogin, GetInfo, GetMenu } from "@/store/modular/user"
import logo from "@/assets/logo.svg"
import './login.less'
import { getToken } from "@/utils/auth";

class Login extends Component{
    state = {
        loading: false
    }

    form = React.createRef()
    onFinish = (values) => {
        this.setState({loading:true})
        this.props.doLogin(values).then((res)=>{
            this.setState({loading:false})
            // if (!res.error){
            //     window.location.href = '/'
            //     message.success("登录成功");
            // }
			this.getUserInfo(res.payload.result.token)
        })
    };
	
	getUserInfo = (token) => {
	    const { GetInfo } = this.props;
	    GetInfo(token).then((data) => {
			console.log("----datadatadata---",data.payload.result)
			//window.location.href = '/'
			message.success("登录成功");
			this.fetchMenuList()
	    }).catch((error) => {
	        message.error(error);
	    });
	}
	fetchMenuList = () => {
		const { GetMenu } = this.props;
		GetMenu().then((data) => {
			console.log("----datadatadata---",data.payload.result)
			window.location.href = '/Home/dashboard'
			message.success("登录成功");
		}).catch((error) => {
		    message.error(error);
		});
	};
    render() {
		//console.log("======this.props====",this.props.token)
		// 如果用户已经登陆, 自动跳转到管理界面
		const token  = getToken()
		//console.log("=====token====",token)
		// if (token) {
		// 	return <Navigate to="/" />;
		// }
        return (
            <>
                <div id="userLayout" className="user-layout-wrapper">
                    <div className="container">
                        <div className="user-layout-lang"></div>
                        <div className="user-layout-content">
                            <div className="top">
                                <div className="header">
                                    <a href="/">
                                        <img src={logo} className="logo" alt="logo"/>
                                        <span className="title">Ant Design</span>
                                    </a>
                                </div>
                                <div className="desc">
                                    Ant Design 是西湖区最具影响力的 Web 设计规范
                                </div>
                            </div>
                            <div className="main">
                                <Form className="user-layout-login" initialValues={{remember: true}} onFinish={this.onFinish} initialValues={{
								username: 'admin',
								password: '8914de686ab28dc22f30d3d8e107ff6c',
							}}>
                                    <Form.Item name="username" rules={[
                                        {
                                            required: true,
                                            message: '请输入登录账号!',
                                        },
                                    ]}>
                                        <Input prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }}/>}  size="large" type="text" placeholder="账号"/>
                                    </Form.Item>
                                    <Form.Item name="password" rules={[
                                        {
                                            required: true,
                                            message: '请输入登录密码!',
                                        },
                                    ]}>
                                        <Input prefix={<LockOutlined style={{ color: 'rgba(0,0,0,.25)' }}/>}  size="large" type="password" placeholder="密码"/>
                                    </Form.Item>
                                    <Form.Item>
                                        <Button block loading={this.state.loading} size="large" type="primary" htmlType="submit">
                                            登录
                                        </Button>
                                    </Form.Item>
                                </Form>
                            </div>
                            <div className="footer">
                                <div className="links">
                                    <a href="_self">帮助</a>
                                    <a href="_self">隐私</a>
                                    <a href="_self">条款</a>
                                </div>
                                <div className="copyright">
                                    Copyright &copy; 2018 vueComponent
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}

export default connect((state) => state.user, { doLogin,GetInfo,GetMenu })(Login)

