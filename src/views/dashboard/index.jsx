import {Component} from "react";
import {Card, Col, Row, Tabs, Tooltip, DatePicker} from "antd";
import {CaretDownOutlined, CaretUpOutlined, InfoCircleOutlined} from "@ant-design/icons";
import './home.less'
import {Chart} from "@antv/g2";

export default class Home extends Component {
    state = {
        loading: false,
        rankList: []
    };

    componentDidMount() {
        const data = [
            {year: '1951 年', sales: 38},
            {year: '1952 年', sales: 52},
            {year: '1956 年', sales: 61},
            {year: '1957 年', sales: 145},
            {year: '1958 年', sales: 48},
            {year: '1959 年', sales: 38},
            {year: '1960 年', sales: 38},
            {year: '1962 年', sales: 38},
        ];
        const chart = new Chart({
            container: 'xsl',
            autoFit: true,
        });
        chart.data(data);
        chart.scale('sales', {
            nice: true,
        });

        chart.tooltip({
            showMarkers: false
        });
        chart.interaction('active-region');

        chart.interval().position('year*sales').color('year', () => {
            return '#2194ff';
        });
        chart.render();
        let rankList = []
        for (let i = 0; i < 7; i++) {
            rankList.push({
                name: '白鹭岛 ' + (i + 1) + ' 号店',
                total: 1234.56 - i * 100
            })
        }

        this.setState({rankList})
    }

    renderCard() {
        return (
            <Card loading={this.state.loading} bodyStyle={{padding: '20px 24px 8px'}} bordered={false}>
                <div className="chart-card-header">
                    <div className="meta">
                        <span className="chart-card-title">总销售额</span>
                        <span className="chart-card-action">
                            <Tooltip title="指标说明">
                                <InfoCircleOutlined/>
                            </Tooltip>
                        </span>
                    </div>
                    <div className="total">
                        ￥126,560
                    </div>
                </div>
                <div className="chart-card-content">
                    <div className="content-fix">
                        <div style={{marginRight: '16px', display: 'inline-block', fontSize: '14px', lineHeight: '22px'}}>
                            <span>
                                周同比
                                <span className="item-text"> 12% </span>
                            </span>
                            <span className='up'>
                                <CaretUpOutlined/>
                            </span>
                        </div>
                        <div style={{marginRight: '16px', display: 'inline-block', fontSize: '14px', lineHeight: '22px'}}>
                            <span>
                                日同比
                                <span className="item-text"> 12% </span>
                            </span>
                            <span className='down'>
                                <CaretDownOutlined/>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="chart-card-footer">
                    <div className="field">
                        日访问量<span> 1234</span>
                    </div>
                </div>
            </Card>
        )
    }

    renderTabBarExtraContent() {
        return (
            <div className="extra-wrapper">
                <div className="extra-item">
                    <span>今日</span>
                    <span>本周</span>
                    <span>本月</span>
                    <span>本年</span>
                </div>
                <DatePicker.RangePicker style={{width: '256px'}}/>
            </div>
        )
    }

    renderRankList(){
        return (
            <>
                <div className="rank">
                    <h4 className="title">门店销售额排名</h4>
                    <ul className="list">
                        {
                            this.state.rankList.map((item,index)=>{
                                return (
                                    <li key={index}>
                                        <span className={index < 3 ? 'active' : null}>{ index + 1 }</span>
                                        <span>{ item.name }</span>
                                        <span>{ item.total }</span>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </>
        )
    }

    render() {
        return (
            <>
                <Row gutter={24}>
                    <Col sm={24} md={12} xl={6} style={{marginBottom: '24px'}}>
                        {this.renderCard()}
                    </Col>
                    <Col sm={24} md={12} xl={6} style={{marginBottom: '24px'}}>
                        {this.renderCard()}
                    </Col>
                    <Col sm={24} md={12} xl={6} style={{marginBottom: '24px'}}>
                        {this.renderCard()}
                    </Col>
                    <Col sm={24} md={12} xl={6} style={{marginBottom: '24px'}}>
                        {this.renderCard()}
                    </Col>
                </Row>

                <Card loading={this.state.loading} bodyStyle={{padding: 0}}>
                    <Tabs tabBarExtraContent={this.renderTabBarExtraContent()} defaultActiveKey='1' size="large" tabBarStyle={{marginBottom: '24px', paddingLeft: '16px'}}>
                        <Tabs.TabPane tab="销售量" key='1'>
                            <Row>
                                <Col xl={16} lg={12} md={12} sm={24} xs={24}>
                                    <div id="xsl" style={{width: '90%', height: '300px', margin: '30px'}}></div>
                                </Col>
                                <Col xl={8} lg={12} md={12} sm={24} xs={24}>{this.renderRankList()}</Col>
                            </Row>
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="访问量" key='2'>
                            <Row>
                                <Col xl={16} lg={12} md={12} sm={24} xs={24}>
                                    {/*<div id="xsl" style={{width: '90%', height: '300px', margin: '30px'}}></div>*/}
                                </Col>
                                <Col xl={8} lg={12} md={12} sm={24} xs={24}>{this.renderRankList()}</Col>
                            </Row>
                        </Tabs.TabPane>
                    </Tabs>
                </Card>
            </>
        )
    }
}
