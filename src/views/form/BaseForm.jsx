import {Component} from "react";
import {Button, Card, DatePicker, Form, Input, InputNumber, Radio} from "antd";
import React from "react";
export default class BaseForm extends Component {
    formRef = React.createRef()
    render() {
        const layout = {
            labelCol: {lg: {span: 7}, sm: {span: 7}},
            wrapperCol: {lg: {span: 10}, sm: {span: 17} }
        };

        return (
            <Card bodyStyle={{padding: '24px 32px'}}>
                <Form {...layout} name="base-form-ref" ref={this.formRef}>
                    <Form.Item label="标题">
                        <Input placeholder="请输入标题"/>
                    </Form.Item>
                    <Form.Item label="起始时间">
                        <DatePicker.RangePicker/>
                    </Form.Item>
                    <Form.Item label="描述">
                        <Input.TextArea rows={4} placeholder="请输入"/>
                    </Form.Item>
                    <Form.Item label="衡量标准">
                        <Input.TextArea rows={4} placeholder="请输入"/>
                    </Form.Item>
                    <Form.Item label="客户">
                        <Input placeholder="请输入"/>
                    </Form.Item>
                    <Form.Item label="邀评人">
                        <Input placeholder="请输入"/>
                    </Form.Item>
                    <Form.Item label="权重">
                        <InputNumber placeholder="请输入"/>
                        <span> %</span>
                    </Form.Item>
                    <Form.Item label="目标公开">
                        <Radio.Group>
                            <Radio value={1}>公开</Radio>
                            <Radio value={2}>部分公开</Radio>
                            <Radio value={3}>不公开</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 0, span: 16 }} style={{textAlign:'center'}}>
                        <Button type="primary" htmlType="submit">Submit</Button>
                        <Button htmlType="button" style={{marginLeft:'10px'}}>Reset</Button>
                    </Form.Item>
                </Form>
            </Card>
        )
    }
}
