import {Component} from "react";
import {Card, Steps} from "antd";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";

export default class StepsForm extends Component{
    state = {
        currentTab: 0
    }

    nextStep = ()=>{
        if (this.state.currentTab < 2) {
            this.setState({
                currentTab:this.state.currentTab+1
            })
        }
    }

    prevStep = ()=>{
        if (this.state.currentTab > 0) {
            this.setState({
                currentTab:this.state.currentTab-1
            })
        }
    }

    finish = ()=>{
        this.setState({
            currentTab:0
        })
    }

    render() {
        return (
            <Card>
                <Steps style={{maxWidth:'750px',margin:'16px auto'}} current={this.state.currentTab}>
                    <Steps.Step title="填写转账信息"/>
                    <Steps.Step title="确认转账信息"/>
                    <Steps.Step title="完成"/>
                </Steps>
                <div className="content">
                    {this.state.currentTab === 0 ? <Step1 nextStep={()=>this.nextStep()}/> : null}
                    {this.state.currentTab === 1 ? <Step2 nextStep={()=>this.nextStep()} prevStep={()=>this.prevStep()}/> : null}
                    {this.state.currentTab === 2 ? <Step3 nextStep={()=>this.nextStep()} finish={()=>this.finish()}/> : null}
                </div>
            </Card>
        )
    }
}
