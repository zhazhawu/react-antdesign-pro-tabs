import React, {Component} from "react";
import {Alert, Button, Form} from "antd";

export default class Step2 extends Component{
    formRef = React.createRef()
    render() {
        const layout = {
            labelCol: {lg: {span: 5}, sm: {span: 5}},
            wrapperCol: {lg: {span: 19}, sm: {span: 19} }
        };
        return (
            <Form {...layout} ref={this.formRef} style={{maxWidth:'500px',margin:'40px auto 0'}}>
                <Alert closable={true} message="确认转账后，资金将直接打入对方账户，无法退回。" style={{marginBottom:'24px'}}/>
                <Form.Item label="付款账户" className="stepFormText">
                    ant-design@alipay.com
                </Form.Item>
                <Form.Item label="收款账户" className="stepFormText">
                    test@example.com
                </Form.Item>
                <Form.Item label="收款人姓名" className="stepFormText">
                    Alex
                </Form.Item>
                <Form.Item label="转账金额" className="stepFormText">
                    ￥ 5,000.00
                </Form.Item>
                <Form.Item wrapperCol={{span: 19, offset: 5}}>
                    <Button type="primary" onClick={()=>this.props.nextStep()}>下一步</Button>
                    <Button style={{marginLeft:'8px'}} onClick={()=>this.props.prevStep()}>上一步</Button>
                </Form.Item>
            </Form>
        )
    }
}


