import React, {Component} from "react";
import {Button, Divider, Form, Input, Select} from "antd";

export default class Step1 extends Component{
    formRef = React.createRef()
    render() {
        const layout = {
            labelCol: {lg: {span: 5}, sm: {span: 5}},
            wrapperCol: {lg: {span: 19}, sm: {span: 19} }
        };
        return (
            <>
                <Form {...layout} ref={this.formRef} style={{maxWidth:'500px',margin:'40px auto 0'}}>
                    <Form.Item label="付款账户" className="stepFormText">
                        <Select placeholder="ant-design@alipay.com">
                            <Select.Option value={1}>ant-design@alipay.com</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="收款账户" className="stepFormText">
                        <Input.Group style={{display:'inline-block',verticalAlign:'middle'}} compact={true}>
                            <Select defaultValue="alipay" style={{width:100}}>
                                <Select.Option value="alipay">支付宝</Select.Option>
                                <Select.Option value="wexinpay">微信</Select.Option>
                            </Select>
                            <Input style={{width:'calc(100% - 100px)'}}/>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label="收款人姓名" className="stepFormText">
                        <Input/>
                    </Form.Item>
                    <Form.Item label="转账金额" className="stepFormText">
                        <Input prefix="￥"/>
                    </Form.Item>
                    <Form.Item wrapperCol={{span: 19, offset: 5}}>
                        <Button type="primary" onClick={()=>this.props.nextStep()}>下一步</Button>
                    </Form.Item>
                </Form>
                <Divider/>
                <div className="step-form-style-desc" style={{padding:'0 56px',color:'rgba(0, 0, 0, .45)'}}>
                    <h3 style={{margin:'0 0 12px',color:'rgba(0, 0, 0, .45)',fontSize:'16px',lineHeight:'32px'}}>说明</h3>
                    <h4 style={{margin:'0 0 4px',fontSize:'14px',lineHeight:'22px'}}>转账到支付宝账户</h4>
                    <p style={{marginTop:0,marginBottom:'12px',lineHeight:'22px'}}>如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。</p>
                    <h4 style={{margin:'0 0 4px',fontSize:'14px',lineHeight:'22px'}}>转账到银行卡</h4>
                    <p style={{marginTop:0,marginBottom:'12px',lineHeight:'22px'}}>如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。</p>
                </div>
            </>
        )
    }
}


