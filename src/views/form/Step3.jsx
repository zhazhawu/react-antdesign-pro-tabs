import {Component} from "react";
import {Button, Col, Result, Row} from "antd";
import "./Step3.less"
export default class Step3 extends Component{

    renderExtra(){
        return (
            <>
                <Button type="primary" onClick={()=>this.props.finish()}>再转一笔</Button>
                <Button style={{marginLeft:8}}>查看账单</Button>
            </>
        )
    }

    render() {
        return (
            <Result status="success" title="操作成功" extra={this.renderExtra()} subTitle="预计两小时内到账" style={{maxWidth:'560px',margin:'40px auto 0'}}>
                <div className="information">
                    <Row>
                        <Col sm={8} xs={24}>付款账户：</Col>
                        <Col sm={16} xs={24}>ant-design@alipay.com</Col>
                    </Row>
                    <Row>
                        <Col sm={8} xs={24}>收款账户：</Col>
                        <Col sm={16} xs={24}>test@example.com</Col>
                    </Row>
                    <Row>
                        <Col sm={8} xs={24}>收款人姓名：</Col>
                        <Col sm={16} xs={24}>辉夜</Col>
                    </Row>
                     <Row>
                        <Col sm={8} xs={24}>转账金额：</Col>
                        <Col sm={16} xs={24}><span className="money">500</span> 元</Col>
                    </Row>
                </div>
            </Result>
        )
    }
}

