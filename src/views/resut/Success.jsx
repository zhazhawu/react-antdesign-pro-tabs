import {Button, Result} from "antd";

function Success(){
    return (
        <Result
            status="success"
            title="Successfully Purchased Cloud Server ECS!"
            subTitle="Order number: 2017182818828182881 Cloud server configuration takes 1-5 minutes, please wait."
            extra={
                <>
                    <Button type="primary">Go Console</Button>
                    <Button>Buy Again</Button>
                </>
            }
        />
    )
}
export default Success

