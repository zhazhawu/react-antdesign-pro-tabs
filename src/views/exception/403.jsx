import {Button, Result} from "antd";

const Result403 = ()=>{
    return (
        <Result status={403} title='403' subTitle="Sorry, you don't have access to this page." extra={<Button type='primary' href='/'>Back Home</Button>} />
    )
}

export default Result403


