import {Component} from "react";
import {Button, Space, Tag} from "antd";
import ProTable from '@ant-design/pro-table';
import {PlusOutlined} from "@ant-design/icons";
import request from "@/utils/request";

export default class TableList extends Component{
    state = {
        columns: [
            {
                title: '标题',
                dataIndex: 'title',
                copyable: true,
                ellipsis: true,
                tip: '标题过长会自动收缩',
                // formItemProps: {
                //     rules: [
                //         {
                //             required: true,
                //             message: '此项为必填项',
                //         },
                //     ],
                // },
                width: '30%',
            },
            {
                title: '状态',
                dataIndex: 'state',
                filters: true,
                onFilter: true,
                valueType: 'select',
                // formItemProps: {
                //     rules: [
                //         {
                //             required: true,
                //             message: '此项为必填项',
                //         },
                //     ],
                // },
                valueEnum: {
                    all: { text: '全部', status: 'Default' },
                    open: {
                        text: '未解决',
                        status: 'Error',
                    },
                    closed: {
                        text: '已解决',
                        status: 'Success',
                        disabled: true,
                    },
                    processing: {
                        text: '解决中',
                        status: 'Processing',
                    },
                },
            },
            {
                title: '标签',
                dataIndex: 'labels',
                search: false,
                // formItemProps: {
                //     rules: [
                //         {
                //             required: true,
                //             message: '此项为必填项',
                //         },
                //     ],
                // },
                renderFormItem: (_, { defaultRender }) => {
                    return defaultRender(_);
                },
                render: (_, record) => (
                    <Space>
                        {record.labels.map(({ name, color }) => (
                            <Tag color={color} key={name}>
                                {name}
                            </Tag>
                        ))}
                    </Space>
                ),
            },
            {
                title: '创建时间',
                key: 'showTime',
                dataIndex: 'created_at',
                valueType: 'date',
                hideInSearch: true,
                // formItemProps: {
                //     rules: [
                //         {
                //             required: true,
                //             message: '此项为必填项',
                //         },
                //     ],
                // },
            },
        ]
    }
    render() {
        return (
            <ProTable
                columns={this.state.columns}
                request={async (params = {}) => request({
                    url:'https://proapi.azurewebsites.net/github/issues',
                    method: 'get',
                    params: params
                })}
                rowKey="id"
                search={{
                    labelWidth: 'auto',
                }}
                form={{
                    ignoreRules: false,
                }}
                pagination={{
                    pageSize: 10,
                }}
                dateFormatter="string"
                headerTitle="高级表格"
                toolBarRender={() => [
                    <Button key="button" icon={<PlusOutlined />} type="primary">
                        新建
                    </Button>,
                ]}
            />
        )
    }
}

