import {Component} from "react";
import {Drawer, Tag, Tooltip} from "antd";
import {connect} from "react-redux"
import {CheckOutlined, CloseOutlined, SettingOutlined} from "@ant-design/icons";
import {colorList, updateTheme} from './settingConfig'
import { toggleTheme, toggleColor, toggleLayoutMode } from "@/store/modular/app";
import "./SettingDrawer.less"

class Index extends Component {
    state = {
        Visible: false
    }

    componentDidMount() {
        if(this.props.color){
            updateTheme(this.props.color)
			this.props.toggleColor(this.props.color)
        }
    }

    showDrawer = ()=>{
        this.setState({
            Visible: true
        })
    }
    close(){
        this.setState({
            Visible: false
        })
    }

    handleMenuTheme(theme) {
        this.props.toggleTheme(theme)
    }

    changeColor(color) {
        if (this.props.color !== color) {
            this.props.toggleColor(color)
            updateTheme(color)
        }
    }

    handleLayout(mode) {
        this.props.toggleLayoutMode(mode)
    }

    render() {
        return (
			<div className="setting-drawer">
				<div className="setting-drawer-index-handles" onClick={()=>this.showDrawer()}><SettingOutlined/></div>
				<Drawer width={300} visible={this.state.Visible} onClose={()=>this.close()} closable={false} placement="right" drawerStyle={{ position: 'absolute' }}>
					<div className="setting-drawer-index-content">
						<div style={{ marginBottom: '24px' }}>
							<h3 className="setting-drawer-index-title">整体风格设置</h3>
							<div className="setting-drawer-index-blockChecbox">
								<Tooltip title="暗色菜单风格">
									<div className="setting-drawer-index-item" onClick={()=>this.handleMenuTheme('dark')}>
										<img src="https://gw.alipayobjects.com/zos/rmsportal/LCkqqYNmvBEbokSDscrm.svg" alt="dark"/>
										{this.props.theme === 'dark' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
								<Tooltip title="亮色菜单风格">
									<div className="setting-drawer-index-item" onClick={()=>this.handleMenuTheme('light')}>
										<img src="https://gw.alipayobjects.com/zos/rmsportal/jpRkZQMyYRryryPNtyIC.svg" alt="light"/>
										{this.props.theme === 'light' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
							</div>
						</div>
						<div style={{ marginBottom: '24px' }}>
							<h3 className="setting-drawer-index-title">主题色</h3>
							<div style={{height:'20px'}}>
								{
									colorList.map(item=>{
										return (
											<Tooltip className="setting-drawer-theme-color-colorBlock" key={item.key} title={item.key}>
												<Tag color={item.color} onClick={()=>this.changeColor(item.color)}>
													{ this.props.color === item.color ? <CheckOutlined/> : null}
												</Tag>
											</Tooltip>
										)
									})
								}

							</div>
						</div>
						<div style={{ marginBottom: '24px' }}>
							<h3 className="setting-drawer-index-title">导航模式</h3>
							<div className="setting-drawer-index-blockChecbox">
								<Tooltip title="侧边栏导航">
									<div className="setting-drawer-index-item" onClick={()=>this.handleLayout('sidemenu')}>
										<img src="https://gw.alipayobjects.com/zos/rmsportal/JopDzEhOqwOjeNTXkoje.svg" alt="sidemenu"/>
										{this.props.layoutMode === 'sidemenu' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
								<Tooltip title="顶部栏导航">
									<div className="setting-drawer-index-item" onClick={()=>this.handleLayout('topmenu')}>
										<img src="https://gw.alipayobjects.com/zos/rmsportal/KDNDBbriJhLwuqMoxcAr.svg" alt="topmenu"/>
										{this.props.layoutMode === 'topmenu' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
								<Tooltip title="混合导航">
									<div className="setting-drawer-index-item" onClick={()=>this.handleLayout('mixmenu')}>
										<img src="https://gw.alipayobjects.com/zos/antfincdn/x8Ob%26B8cy8/LCkqqYNmvBEbokSDscrm.svg" alt="mixmenu"/>
										{this.props.layoutMode === 'mixmenu' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
								<Tooltip title="侧边混合">
									<div className="setting-drawer-index-item" onClick={()=>this.handleLayout('mixside')}>
										<img src="https://gw.alipayobjects.com/zos/antfincdn/x8Ob%26B8cy8/LCkqqYNmvBEbokSDscrm.svg" alt="mixside"/>
										{this.props.layoutMode === 'mixside' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
							</div>
							<div className="setting-drawer-index-blockChecbox" style={{ marginTop: '10px' }}>
								<Tooltip title="混合导航">
									<div className="setting-drawer-index-item" onClick={()=>this.handleLayout('mix')}>
										<img src="https://gw.alipayobjects.com/zos/antfincdn/x8Ob%26B8cy8/LCkqqYNmvBEbokSDscrm.svg" alt="mix"/>
										{this.props.layoutMode === 'mix' ? <div className="setting-drawer-index-selectIcon"><CheckOutlined/></div> : null}
									</div>
								</Tooltip>
							</div>
						</div>
					</div>
					{ this.state.Visible ? <div className="setting-drawer-index-handle" onClick={()=>this.close()}><CloseOutlined/></div> : <div className="setting-drawer-index-handle" onClick={()=>this.showDrawer()}><SettingOutlined/></div>}
				</Drawer>
			</div>
        )
    }
}
export default connect((state)=>state.app,{ toggleTheme, toggleColor, toggleLayoutMode },null, {forwardRef:true})(Index)
