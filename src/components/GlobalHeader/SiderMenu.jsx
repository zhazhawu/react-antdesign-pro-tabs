import React, {Component} from "react";
import { connect } from "react-redux"
import {Link} from "react-router-dom";
import {Layout} from "antd";
import Menu from "./Menu";

class SiderMenu extends Component {
    render() {
        return (
            <Layout.Sider width={256} theme={this.props.theme} trigger={null} id='basic-layout-side' collapsible collapsed={this.props.collapsed} collapsedWidth={48}>
                <div className="logo" style={{padding: `16px ${!this.props.collapsed ? 16 : 8}px`}}>
                    <Link to='/'>
                        <img src="https://iczer.gitee.io/vue-antd-admin/static/img/logo.9652507e.png" alt="es-lint want to get"/>
                        {!this.props.collapsed ? <h1 className={ this.props.theme === 'light' ? 'theme-light-title' : ''}>Ant Design Pro</h1> : null}
                    </Link>
                </div>
                <Menu menus={this.props.menus} onBreadcrumb={()=>this.props.onBreadcrumb}/>
            </Layout.Sider>
        )
    }
}

export default connect((state) => state.app)(SiderMenu)
