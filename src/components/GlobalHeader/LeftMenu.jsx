import { useEffect, useState } from 'react';
import {Menu} from "antd";
import {Link, useLocation, useNavigate} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { setUserItem } from '@/store/modular/user';
import { getFirstPathCode } from '@/utils/getFirstPathCode';
import {treeToArray} from "@/utils/utils";
import {CheckCircleOutlined, FormOutlined, HomeOutlined, TableOutlined, WarningOutlined} from "@ant-design/icons"

const { SubMenu, Item } = Menu;

const M = props => {
	const { theme, route, memuData } = props;
	const {pathname} = useLocation();
	const { layoutMode } = useSelector(state => state.app);
	const [openKey, setOpenkey] = useState();
	const [selectedKey, setSelectedKey] = useState(pathname);
	const navigate = useNavigate();
	const dispatch = useDispatch();
	
	useEffect(() => {
		let match
		const str = pathname.lastIndexOf("/");
		const path = pathname.slice(str + 1);
		const key = getFirstPathCode(pathname)
		if(route.length !== 0){
			match = route.find(item => item.key === key)
			if(pathname === '/Home/dashboard'){
				setSelectedKey(route[0].key)
				setOpenkey(match.key);
			}else{
				setOpenkey(key);
				//setSelectedKey(path)
				setSelectedKey(key)
			}
		}
	
		
	}, [pathname,layoutMode]);

	const getTitie = (menu) => {
		return (
			<>
				<span>{Icon(menu.icon)}</span>
				<div>{menu.title}</div>
			</>
			
		);
	};
	
	const Icon = (type) => {
		let icon
		if (type === 'HomeOutlined') {
			icon = <HomeOutlined />;
		} else if (type === 'FormOutlined') {
			icon = <FormOutlined />;
		} else if (type === 'TableOutlined') {
			icon = <TableOutlined />;
		} else if (type === 'CheckCircleOutlined') {
			icon = <CheckCircleOutlined />;
		} else if (type === 'CheckCircleOutlined') {
			icon = <CheckCircleOutlined />;
		} else if (type === 'WarningOutlined') {
			icon = <WarningOutlined />;
		}
		return icon;
	};

	const onMenuClick = (path) => {
		setSelectedKey(path)
	};

	const onOpenChange = (keys) => {
		const key = keys.pop();
		setOpenkey(key)
	};

	return (
		<Menu
			mode="inline"
			theme={theme}
			selectedKeys={[selectedKey]}
			openKeys={openKey ? [openKey] : []}
			onOpenChange={onOpenChange}
			onSelect={k => onMenuClick(k.key)}
		>
			{memuData && memuData.map(menu =>
				<Menu.Item key={menu.key}>
				    <Link to={menu.path}>{getTitie(menu)}</Link>
				</Menu.Item>
			)}
		</Menu>
	);
};

export default M;
