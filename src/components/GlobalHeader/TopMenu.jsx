import {Link} from "react-router-dom";
import { useSelector } from 'react-redux';
import { RouterMap } from "@/config/route.config";
import Menu from './Tmenu';


const TopMenu = props => {
	const { layoutMode, collapsed } = useSelector(state => state.app);
	const { theme, route, memuData } = props;
	return (
		<>
			{layoutMode === 'topmenu' || layoutMode === 'mixside'? 
			<div className="logo" style={{padding: `16px`,width: `${layoutMode !== 'sidemenu' ? (collapsed ? 60 : 256) : 0}px`,display:'inline-block',lineHeight:64,minHeight:64}}>
			    <Link to='/'>
			        <img src="https://iczer.gitee.io/vue-antd-admin/static/img/logo.9652507e.png" alt="es-lint want to get"/>
			        <h1 className={ theme === 'light' ? 'theme-light-title' : ''}>Ant Design Pro</h1>
			    </Link>
			</div>
			: null}
			<div style={{display:'inline-block', transition: '0.2s all'}}>
				<Menu mode="horizontal" route={route} memuData={memuData} theme={theme} />
			</div>
		</>
	);
}


export default TopMenu
