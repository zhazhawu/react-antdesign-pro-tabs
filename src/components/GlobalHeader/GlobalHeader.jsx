import React, {Component} from "react";
import { connect } from "react-redux"
import {Avatar, Badge, Dropdown, Menu, Tooltip, Layout, Modal} from "antd";
import {BellOutlined, ExclamationCircleOutlined, MenuFoldOutlined, MenuUnfoldOutlined, SettingOutlined} from "@ant-design/icons";
import { toggleCollapsed } from "@/store/modular/app";
import { Logout, GetInfo } from "@/store/modular/user";
import SettingDrawer from "@/components/SettingDrawer/SettingDrawer"
import TopMenu from "./TopMenu";
import { getToken } from "@/utils/auth";

class GlobalHeader extends Component {
    SettingDrawerRef = React.createRef()

    componentDidMount() {
        //this.props.GetInfo()
		//console.log("=======this.props=========",this.props)
    }
	
    logout(){
        const _this = this
		const token = getToken()
        Modal.confirm({
            title: '信息',
            icon: <ExclamationCircleOutlined />,
            content: '您确定要注销吗？',
            onOk() {
                _this.props.Logout(token)
            },
        })
    }

    //@returns {JSX.Element}
    renderRightContent(){
        const menu = (
            <Menu className='ant-pro-drop-down menu'>
                <Menu.Item key="1">个人中心</Menu.Item>
                <Menu.Item key="2">账户设置</Menu.Item>
				<Menu.Item key="3" disabled>测试</Menu.Item>
				<Menu.Divider />
                <Menu.Item key="4" onClick={() => this.logout()}>退出登录</Menu.Item>
            </Menu>
        );
        return (
            <div className="ant-pro-global-header-index-right ant-pro-global-header-index-light" id="right-content">
                <ul>
                    <li>
                        <Tooltip placement="bottom" title='设置'>
                            <SettingOutlined onClick={()=>this.SettingDrawerRef.current.showDrawer()} style={{
                                fontSize:'16px',
                                color: (this.props.layoutMode !== 'sidemenu' && this.props.theme === 'dark') ? '#fff' : '#262626'
                            }}/>
                        </Tooltip>
                    </li>
                    <li>
                        <Tooltip placement="bottom" title='消息通知'>
                            <Badge count={10} size="small">
                                <BellOutlined style={{
                                    fontSize:'16px',
                                    color: (this.props.layoutMode !== 'sidemenu' && this.props.theme === 'dark') ? '#fff' : '#262626'
                                }}/>
                            </Badge>
                        </Tooltip>
                    </li>
                    <li>
                        <Dropdown overlay={menu}>
                            <div className="ant-pro-account-avatar">
                                <Avatar size="small" src={this.props.info.avatar} className="antd-pro-global-header-index-avatar"/>
                                <span style={{
                                    color: (this.props.layoutMode !== 'sidemenu' && this.props.theme === 'dark') ? '#fff' : '#262626'
                                }}>{this.props.info.name}</span>
                            </div>
                        </Dropdown>
                    </li>
                </ul>
            </div>
        )
    }
    render() {
        return (
            <>
                <Layout.Header className="header" style={{
                    width: '100%' ,
                    transition: '0.2s all',
                    background: (this.props.theme !== 'dark' || this.props.layoutMode === 'sidemenu' || this.props.layoutMode === 'mixside') ? '#fff' : '#001529'
                }}>
                    {this.props.layoutMode === 'sidemenu' || this.props.layoutMode === 'mixside' ? React.createElement(this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: ()=>this.props.toggleCollapsed(!this.props.collapsed),
                    }) : null}
                    {this.props.layoutMode !== 'sidemenu' && this.props.layoutMode !== 'mixside' ? <TopMenu route={this.props.route} memuData={this.props.memuData} firstMenu={this.props.firstMenu} onBreadcrumb={()=>this.props.onBreadcrumb}/> : null}
                    {this.renderRightContent()}
                </Layout.Header>
                <SettingDrawer ref={this.SettingDrawerRef}>
					<SettingOutlined onClick={()=>this.SettingDrawerRef.current.showDrawer()} style={{
					    fontSize:'16px',
					    color: (this.props.layoutMode !== 'sidemenu' && this.props.theme === 'dark') ? '#fff' : '#262626'
					}}/>
				</SettingDrawer>
            </>
        )
    }
}
export default connect((state)=>{
    return {...state.app,...state.user}
}, { toggleCollapsed, Logout, GetInfo })(GlobalHeader)
