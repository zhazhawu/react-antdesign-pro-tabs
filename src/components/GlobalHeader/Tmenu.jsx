import { useEffect, useState } from 'react';
import {Menu} from "antd";
import {Link, useLocation, useNavigate} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { setMenu } from '@/store/modular/user';
import { getFirstPathCode } from '@/utils/getFirstPathCode';
import {treeToArray} from "@/utils/utils";
import {CheckCircleOutlined, FormOutlined, HomeOutlined, TableOutlined, WarningOutlined} from "@ant-design/icons"

const { SubMenu, Item } = Menu;

const M = props => {
	const { route, memuData, mode } = props;
	const {pathname} = useLocation();
	const { theme, layoutMode } = useSelector(state => state.app);
	const [openKey, setOpenkey] = useState();
	const [selectedKey, setSelectedKey] = useState(pathname);
	const navigate = useNavigate();
	const dispatch = useDispatch();
	//console.log("====menuListmenuListmenuList===",menuList)
	useEffect(() => {
		console.log("====memuDatamemuDatamemuData===",memuData)
		console.log("====pathnamepathnamepathname===",pathname)
		console.log("====routerouteroute===",route)
		const key = getFirstPathCode(pathname)
		const match = route.find(item => item.key === key)
		
		console.log("====matchmatchmatch===",match)
		if(layoutMode === 'mixmenu'){
			dispatch(setMenu(match.children))
		}
		if(pathname === '/'){
			setSelectedKey(memuData[0].key)
		}else{
			if(layoutMode === 'topmenu'){
				const str = pathname.lastIndexOf("/");
				const keypath = pathname.slice(str + 1);
				setSelectedKey(keypath)
			}else{
				//setSelectedKey(match.key)
				setSelectedKey(key)
			}
			
		}
		//console.log("====pathnamepathnamepathname===",pathname)
		
	}, [pathname,layoutMode]);

	const getTitie = (menu) => {
		return (
			<span>{menu.title}</span>
		);
	};
	
	const Icon = (type) => {
		let icon
		if (type === 'HomeOutlined') {
			icon = <HomeOutlined />;
		} else if (type === 'FormOutlined') {
			icon = <FormOutlined />;
		} else if (type === 'TableOutlined') {
			icon = <TableOutlined />;
		} else if (type === 'CheckCircleOutlined') {
			icon = <CheckCircleOutlined />;
		} else if (type === 'CheckCircleOutlined') {
			icon = <CheckCircleOutlined />;
		} else if (type === 'WarningOutlined') {
			icon = <WarningOutlined />;
		}
		return icon;
	};

	const onMenuClick = (path) => {
		console.log("====pathpathpath===",path)
		let match
		//console.log("====routesroutesroutes===",routes)
		if(layoutMode === 'topmenu'){
			match = route.find(item => item.key === path)
		}else{
			match = route.find(item => item.key === path)
		}
		console.log("====match===",match)
		//dispatch(setMenu(match.children))
		const str = path.lastIndexOf("/");
		const key = path.slice(str + 1);
		setSelectedKey(key)
	};

	const onOpenChange = (keys) => {
		const key = keys.pop();
		setOpenkey(key)
	};

	return (
		<Menu
			mode={mode}
			theme={theme}
			selectedKeys={[selectedKey]}
			openKeys={openKey ? [openKey] : []}
			onOpenChange={onOpenChange}
			onSelect={k => onMenuClick(k.key)}
		>
			{memuData.map(menu =>
				menu.children && layoutMode === 'topmenu' ? (
					<Menu.SubMenu key={menu.key} icon={Icon(menu.icon)} title={<span>{menu.title}</span>}>
						{menu.children.map(child => (
							<Menu.Item key={child.key} icon={Icon(child.icon)}>
								<Link to={child.path}>{child.title}</Link>
							</Menu.Item>
						))}
					</Menu.SubMenu>
				) : (
					<Menu.Item key={menu.key} icon={Icon(menu.icon)}>
					   <Link to={menu.path}>{menu.title}</Link>
					</Menu.Item>
				),
			)}
		</Menu>
	);
};

export default M;
